<!DOCTYPE html>
	<head>
		<title>Belajar HTML dan CSS</title>
		<link rel="stylesheet" href="aboutstyle.css"/>
	</head>
	<body>
		<div class="Badan">
			<div class="header">
				<div class="logo">
					<div class="inlogo">
						<img src="logo.jpg">
					</div>
					<div class="inlogo2">
						<h2>KarizaTravel.id</h2>
					</div>
				</div>
				<div class="menu">
					<div class="portfolio">
						<a href="portfolio.php"><h3>PORTFOLIO</h3></a>
					</div>
					<div class="portfolio">
						<a href="about.php"><h3>ABOUT</h3></a>
					</div>
					<div class="portfolio">
						<a href="contact.php"><h3>CONTACT</h3></a>
					</div>
					<div class="portfolio">
						<a href="destination.php"><h3>DESTINATION</h3></a>
					</div>
					<div class="portfolio">
						<a href="index.php"><h3>HOME</h3></a>
					</div>
				</div>
			</div>
			
			<div class="about">
				<h2>ABOUT</h2>
			</div>
			<div class="undabout">
				<h2>KATSRUR RIZQI  AVIVA</h2>
			</div>
			<div class="tempabout">
				<div class="aboutme">
					<h3>Sebagai salah satu pelopor web,
						KarizaTravel.id berkomitmen memberikan kemudahan
						untuk mencari tempat wisata serta penginapan
						online yang aman, nyaman, mudah, menyenangkan, dimana
						saja dan kapan saja.</h3>
				</div>
				<div class="abme">
					<h3> FAQ<br>
						Q: Apakah KarizaTravel.id memiliki stan offline?<br>
						A: Saat ini KarizaTravel.id belum memiliki stan offline.
						Pemesanan hanya bisa dilakukan melalui website KarizaTravel.id.<br>

						Q: Apakah KarizaTravel.id termasuk dalam kategori situs penanaman modal?<br>
						A: KarizaTravel.id dapat dijadikan untuk penanaman modal secara online.<br>
						
						Q: Apakah aman dan terjamin proses pemesanan di KarizaTravel.id?<br>
						A: KarizaTravel.id bekerja sama dengan mitra perbankan terpercaya dan
						semua transaksi dijamin keamanannya dengan sertifikasi VeriSign,
						Verified by VISA, MasterCard SecureCode dan Credit Card Fraud Detection System.<br>
						</h3>
				</div>
			</div>
		</div>
	</body>
	
</html>